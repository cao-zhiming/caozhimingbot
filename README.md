# Caozhimingbot

This is a JS software for a Keybase Bot [CaozhimingBot, also known as Cecilia Keybot](https://keybase.io/caozhimingbot).

This bot functions by directly operating to the Keybase CLI on the server so it gets and sends messages.
Here is the docs for using.

![Status](https://img.shields.io/uptimerobot/status/m788744221-4681e999eb1f7e09c855f2c4)

To be continued...
